package com.vouchers.vouchersapi.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vouchers.vouchersapi.models.CVoucher;
import com.vouchers.vouchersapi.repository.IVoucherRepository;


@CrossOrigin
@RestController
@RequestMapping
public class CVoucherController {
    @Autowired
    IVoucherRepository pVoucherRepository;

    // Viết method get list voucher: getAllVouchers()
    @GetMapping("/vouchers")
    public ResponseEntity<List<CVoucher>> getAllVouchers() {
        try {
            List<CVoucher> listVouchers = new ArrayList<CVoucher>();
            pVoucherRepository.findAll().forEach(listVouchers::add);
            return new ResponseEntity<>(listVouchers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Viết method get list voucher: getVoucherById()
    @GetMapping("/vouchers/{id}")
	public ResponseEntity<CVoucher> getCVoucherById(@PathVariable("id") long id) {
        try {
                Optional<CVoucher> voucherData = pVoucherRepository.findById(id);
            if (voucherData.isPresent()) {
                return new ResponseEntity<>(voucherData.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Viết method create voucher: createVoucher()
    @PostMapping("/vouchers")
    public ResponseEntity<CVoucher> createCVoucher(@RequestBody CVoucher pVouchers) {
        try {
            pVouchers.setNgayTao(new Date());
            pVouchers.setNgayCapNhat(null);

            CVoucher _vouchers = pVoucherRepository.save(pVouchers);
            return new ResponseEntity<>(_vouchers, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Viết method update voucher: updateVoucher()
    @PutMapping("/vouchers/{id}")
    public ResponseEntity<CVoucher> updateCVoucherById(@PathVariable("id") long id, @RequestBody CVoucher pVouchers) {
        try {
            Optional<CVoucher> voucherData = pVoucherRepository.findById(id);

        if (voucherData.isPresent()) {
            CVoucher voucher = voucherData.get();
            voucher.setMaVoucher(pVouchers.getMaVoucher());
            voucher.setGhiChu(pVouchers.getGhiChu());
            voucher.setNgayCapNhat(new Date());
            voucher.setPhanTramGiamGia(pVouchers.getPhanTramGiamGia());
            return new ResponseEntity<>(pVoucherRepository.save(voucher), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // Viết method delete voucher: deleteVoucher()
    @DeleteMapping("/vouchers/{id}")
	public ResponseEntity<CVoucher> deleteCVoucherById(@PathVariable("id") long id) {
		try {
			pVoucherRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


}

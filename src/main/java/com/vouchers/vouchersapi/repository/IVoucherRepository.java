package com.vouchers.vouchersapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vouchers.vouchersapi.models.CVoucher;

public interface IVoucherRepository extends JpaRepository <CVoucher, Long>{
    
}


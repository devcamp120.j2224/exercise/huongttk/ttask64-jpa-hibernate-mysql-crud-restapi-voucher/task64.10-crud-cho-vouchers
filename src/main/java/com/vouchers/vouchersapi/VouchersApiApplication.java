package com.vouchers.vouchersapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VouchersApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(VouchersApiApplication.class, args);
	}

}
